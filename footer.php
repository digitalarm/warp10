		</main>
		<?php if ( ! is_page_template( 'page-templates/page-splash.php' ) ) : ?>

			<footer class="footer">
				<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>

					<div class="footer__nav">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => 'nav', 'container_class' => 'container' ) ); ?>
					</div>

				<?php endif; ?>

				<?php get_template_part( 'templates/template-parts/social-icons' ); ?>

				<p class="footer__copy">
					<?php echo str_replace( 'DATE', date( 'Y' ), get_theme_mod( 'w10_copyright_text', '&copy; Company Name DATE' ) ); ?>
				</p>
			</footer>

		<?php endif; ?>

		<?php if ( has_nav_menu( 'header-menu' ) ) : ?>

			<div class="mobile-menu">
				<span class="mobile-menu__close" aria-label="<?php _e( 'Close Menu', 'w10' ); ?>">&times;</span>
				<?php wp_nav_menu( array( 'theme_location' => 'mobile-menu', 'container' => 'nav' ) ); ?>
			</div>

		<?php endif; ?>

		<div class="cookie-bar"><?php _e( "If you use our website we'll assume you’re happy for us to use cookies.", 'w10'); ?><span class="cookie-bar__close button">X</span></div>
		<?php if ( TRUE === get_theme_mod( 'gmaps_enabled', FALSE ) && '' !== get_theme_mod( 'gmaps_api_key', '' ) ) : ?>

			<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo get_theme_mod( 'gmaps_api_key', '' ); ?>"></script>

		<?php endif; ?>

		<?php wp_footer(); ?>
	</body>
</html>
