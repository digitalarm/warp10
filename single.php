<?php get_header(); ?>

<div class="blog-posts spacing-inside">
	<div class="container">
		<div class="page-content">
			<?php while ( have_posts() ) : the_post(); ?>

				<article class="article" id="post-<?php the_ID(); ?>">
					<div class="article__header">
						<div class="article__info">
							<h1><?php the_title(); ?></h1>
							<span class="article__date"><?php printf( __( 'Posted on %s in ', 'w10' ), get_the_time( 'd M Y' ) ); the_category( ', ' ); echo $author; ?></span>
						</div>

						<?php if ( TRUE === get_theme_mod( 'w10_author_image', FALSE ) ) : ?>

							<div class="article__author">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 96 ); ?>

								<?php if ( TRUE === get_theme_mod( 'w10_author_name', FALSE ) ) : ?>

									<span><?php printf( __( 'by %s' ), get_the_author() ); ?></span>
								<?php endif; ?>

							</div>

						<?php elseif ( TRUE === get_theme_mod( 'w10_author_name', FALSE ) ) : ?>

							<div class="article__author">
								<?php $author = sprintf( __( ' by %s' ), get_the_author() ); ?>
							</div>

						<?php endif; ?>
					</div>

					<div class="article__text"><?php the_content(); ?></div>
				</article>

				<?php if ( TRUE === get_theme_mod( 'enable_comments', TRUE ) ) :

					comments_template();

				endif; ?>

			<?php endwhile; ?>
		</div>

		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
