<?php if ( get_field( 'facebook_link', 'option' ) || get_field( 'twitter_link', 'option' ) || get_field( 'instagram_link', 'option' ) || get_field( 'youtube_link', 'option' ) || get_field( 'googleplus_link', 'option' ) || get_field( 'snapchat_link', 'option' ) || get_field( 'flickr_link', 'option' ) || get_field( 'linkedin_link', 'option' ) || get_field( 'pinterest_link', 'option' ) ) : ?>

	<div class="social-links">
		<div class="container">
			<?php if ( get_field( 'facebook_link', 'option' ) ) : ?><a class="social-link facebook-link" href="<?php the_field( 'facebook_link', 'option' ); ?>" target="_blank"><span class="fa fa-facebook"></span></a><?php endif; ?>
			<?php if ( get_field( 'twitter_link', 'option' ) ) : ?><a class="social-link twitter-link" href="<?php the_field( 'twitter_link', 'option' ); ?>" target="_blank"><span class="fa fa-twitter"></span></a><?php endif; ?>
			<?php if ( get_field( 'instagram_link', 'option' ) ) : ?><a class="social-link instagram-link" href="<?php the_field( 'instagram_link', 'option' ); ?>" target="_blank"><span class="fa fa-instagram"></span></a><?php endif; ?>
			<?php if ( get_field( 'youtube_link', 'option' ) ) : ?><a class="social-link youtube-link" href="<?php the_field( 'youtube_link', 'option' ); ?>" target="_blank"><span class="fa fa-youtube"></span></a><?php endif; ?>
			<?php if ( get_field( 'googleplus_link', 'option' ) ) : ?><a class="social-link googleplus-link" href="<?php the_field( 'googleplus_link', 'option' ); ?>" target="_blank"><span class="fa fa-google-plus"></span></a><?php endif; ?>
			<?php if ( get_field( 'snapchat_link', 'option' ) ) : ?><a class="social-link snapchat-link" href="<?php the_field( 'snapchat_link', 'option' ); ?>" target="_blank"><span class="fa fa-bell"></span></a><?php endif; ?>
			<?php if ( get_field( 'flickr_link', 'option' ) ) : ?><a class="social-link flickr-link" href="<?php the_field( 'flickr_link', 'option' ); ?>" target="_blank"><span class="fa fa-flickr"></span></a><?php endif; ?>
			<?php if ( get_field( 'linkedin_link', 'option' ) ) : ?><a class="social-link linkedin-link" href="<?php the_field( 'linkedin_link', 'option' ); ?>" target="_blank"><span class="fa fa-linkedin"></span></a><?php endif; ?>
			<?php if ( get_field( 'pinterest_link', 'option' ) ) : ?><a class="social-link pinterest-link" href="<?php the_field( 'pinterest_link', 'option' ); ?>" target="_blank"><span class="fa fa-pinterest"></span></a><?php endif; ?>
		</div>
	</div>

<?php endif; ?>
