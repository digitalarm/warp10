<?php
/**
 * Template Name: Splash
 *
 * Description: Splash page template.
 *
 */

get_header();
?>

<?php if ( have_posts() ) :  ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'templates/template-parts/content-blocks' ); ?>

	<?php endwhile; ?>

<?php endif; ?>

<?php get_footer(); ?>
