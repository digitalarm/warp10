<?php $image = get_sub_field( 'image' ); ?>

<div class="block-parallax spacing-<?php the_sub_field( 'spacing' ); ?>">
	<div class="block-parallax__image" style="background-image:url('<?php echo $image['url']; ?>');">
		<div class="block-parallax__text"><?php the_sub_field( 'text_overlay' ); ?></div>
	</div>
</div>
