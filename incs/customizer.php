<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
* Register Theme Customizer
*
* @param object $wp_customize An instance of the WP_Customize_Manager class
*
* @return Customizer settings and controls
*/
function w10_register_theme_customizer( $wp_customize ) {

	$wp_customize->add_section(
		'w10_post_settings',
		array(
			'title'		=> __( 'Single Post Settings', 'w10' ),
			'priority'	=> 200
		)
	);

	$wp_customize->add_setting(
		'w10_author_name',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'w10_author_name',
		array(
			'section'		=> 'w10_post_settings',
			'label'			=> __( 'Show Author Name', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'w10_author_image',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'w10_author_image',
		array(
			'section'		=> 'w10_post_settings',
			'label'			=> __( 'Show Author Avatar', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_template_fields',
		array(
			'title'		=> __( 'Template Fields', 'w10' ),
			'priority'	=> 201
		)
	);

	$wp_customize->add_setting(
		'w10_copyright_text',
		array(
			'default'	=> '&copy; Company Name DATE'
		)
	);

	$wp_customize->add_control(
		'w10_copyright_text',
		array(
			'section'		=> 'w10_template_fields',
			'label'			=> __( 'Footer Copyright Text', 'w10' ),
			'description'	=> '\'DATE\' ' . __( 'will be replaced with the current year', 'w10' ),
			'type'			=> 'textarea'
		)
	);

	$wp_customize->add_section(
		'w10_custom_javascript',
		array(
			'title'		=> __( 'Custom Javascript', 'w10' ),
			'priority'	=> 202
		)
	);

	$wp_customize->add_setting(
		'w10_header_javascript',
		array(
			'default'	=> ''
		)
	);

	$wp_customize->add_control(
		'w10_header_javascript',
		array(
			'section'	=> 'w10_custom_javascript',
			'label'		=> __( 'Header Javascript', 'w10' ),
			'type'		=> 'textarea'
		)
	);

	$wp_customize->add_setting(
		'w10_footer_javascript',
		array(
			'default'	=> ''
		)
	);

	$wp_customize->add_control(
		'w10_footer_javascript',
		array(
			'section'	=> 'w10_custom_javascript',
			'label'		=> __( 'Footer Javascript', 'w10' ),
			'type'		=> 'textarea'
		)
	);

	$wp_customize->add_panel(
		'w10_site_settings',
		array(
			'title'		=> __( 'Site Settings', 'w10' ),
			'priority'	=> 203
		)
	);

	$wp_customize->add_section(
		'w10_admin',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Admin', 'w10' ),
			'priority'	=> 1
		)
	);

	$wp_customize->add_setting(
		'show_admin_bar',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'show_admin_bar',
		array(
			'section'	=> 'w10_admin',
			'label'		=> __( 'Show Admin Bar', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_libraries',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Libraries', 'w10' ),
			'priority'	=> 2
		)
	);

	$wp_customize->add_setting(
		'waitforimages_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'waitforimages_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Wait For Images Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'isotope_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'isotope_enabled',
		array(
			'section'		=> 'w10_libraries',
			'label'			=> __( 'Isotope Enabled', 'w10' ),
			'description'	=> __( '(Requires waitForImages)', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'slick_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'slick_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Slick Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'magnificpopup_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'magnificpopup_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Magnific Popup Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'hoverizr_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'hoverizr_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Hoverizr Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'fontawesome_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'fontawesome_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Font Awesome Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'wow_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'wow_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Wow JS Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'animate_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'animate_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Animate CSS Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'lazyload_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'lazyload_enabled',
		array(
			'section'		=> 'w10_libraries',
			'label'			=> __( 'Lazyload Enabled', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_gmaps',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Google Maps', 'w10' ),
			'priority'	=> 3
		)
	);

	$wp_customize->add_setting(
		'gmaps_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'gmaps_enabled',
		array(
			'section'		=> 'w10_gmaps',
			'label'			=> __( 'Google Maps Enabled', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'gmaps_api_key',
		array(
			'default'	=> ''
		)
	);

	$wp_customize->add_control(
		'gmaps_api_key',
		array(
			'section'		=> 'w10_gmaps',
			'label'			=> __( 'Google Maps API Key', 'w10' ),
			'type'			=> 'text'
		)
	);

	$wp_customize->add_section(
		'w10_images',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Images', 'w10' ),
			'priority'	=> 4
		)
	);

	$wp_customize->add_setting(
		'images_as_links',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'images_as_links',
		array(
			'section'		=> 'w10_images',
			'label'			=> __( 'Link Images By Default', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_comments',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Comments', 'w10' ),
			'priority'	=> 5
		)
	);

	$wp_customize->add_setting(
		'enable_comments',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'enable_comments',
		array(
			'section'		=> 'w10_comments',
			'label'			=> __( 'Enable Comments', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'enable_pingbacks',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'enable_pingbacks',
		array(
			'section'		=> 'w10_comments',
			'label'			=> __( 'Enable Pingbacks', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'enable_threaded_comments',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'enable_threaded_comments',
		array(
			'section'		=> 'w10_comments',
			'label'			=> __( 'Enable Threaded Comments', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_sidebar',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Sidebar', 'w10' ),
			'priority'	=> 4
		)
	);

	$wp_customize->add_setting(
		'fixed_sidebar',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'fixed_sidebar',
		array(
			'section'		=> 'w10_sidebar',
			'label'			=> __( 'Fixed Sidebar', 'w10' ),
			'description'	=> __( 'Sidebar will follow the window when scrolling', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_sitemap',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Sitemap', 'w10' ),
			'priority'	=> 6
		)
	);

	$wp_customize->add_setting(
		'sitemap_authors',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'sitemap_authors',
		array(
			'section'		=> 'w10_sitemap',
			'label'			=> __( 'Show Authors', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'sitemap_pages',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'sitemap_pages',
		array(
			'section'		=> 'w10_sitemap',
			'label'			=> __( 'Show Pages', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'sitemap_posts',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'sitemap_posts',
		array(
			'section'		=> 'w10_sitemap',
			'label'			=> __( 'Show Posts', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'sitemap_custom_posts',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'sitemap_custom_posts',
		array(
			'section'		=> 'w10_sitemap',
			'label'			=> __( 'Show Custom Posts', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_head_cleanup',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Head Cleanup', 'w10' ),
			'priority'	=> 7
		)
	);

	$wp_customize->add_setting(
		'rm_shortlink',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'rm_shortlink',
		array(
			'section'		=> 'w10_head_cleanup',
			'label'			=> __( 'Remove Shortlink', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'rm_rsd_link',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'rm_rsd_link',
		array(
			'section'		=> 'w10_head_cleanup',
			'label'			=> __( 'Remove RSD Link', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'rm_wlwmanifest_link',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'rm_wlwmanifest_link',
		array(
			'section'		=> 'w10_head_cleanup',
			'label'			=> __( 'Remove WLW Manifest Link', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'rm_emoji_scripts',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'rm_emoji_scripts',
		array(
			'section'		=> 'w10_head_cleanup',
			'label'			=> __( 'Remove Emoji Scripts', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'rm_emoji_styles',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'rm_emoji_styles',
		array(
			'section'		=> 'w10_head_cleanup',
			'label'			=> __( 'Remove Emoji Styles', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

}
add_action( 'customize_register', 'w10_register_theme_customizer' );


/**
* Customizer Head
*
* @return Customizer content to be added to <head>
*/
function w10_customizer_head() {

	if ( current_user_can( 'manage_options' ) ) {

		return;

	}

	echo get_theme_mod( 'w10_header_javascript', '' );

}
add_action( 'wp_head', 'w10_customizer_head' );


/**
* Customizer Footer
*
* @return Customizer content to be added to the bottom of the document
*/
function w10_customizer_footer() {

	if ( current_user_can( 'manage_options' ) ) {

		return;

	}

	echo get_theme_mod( 'w10_footer_javascript', '' );

}
add_action( 'wp_footer', 'w10_customizer_footer' );


// Load scripts
function w10_load_lib_scripts() {

	if ( TRUE === get_theme_mod( 'waitforimages_enabled', FALSE ) ) {

		wp_enqueue_script( 'waitforimages', LIB_PATH . '/jquery/jquery.waitforimages.js', array('jquery'), '2.1.0', TRUE );

	}

	if ( TRUE === get_theme_mod( 'slick_enabled', TRUE ) ) {

		wp_enqueue_script( 'slick-js', LIB_PATH . '/slick/slick.min.js', array('jquery'), '1.6.0', TRUE );

	}

	if ( TRUE === get_theme_mod( 'magnificpopup_enabled', TRUE ) ) {

		wp_enqueue_script( 'magnificpopup-js', LIB_PATH . '/magnificpopup/magnific-popup.js', array('jquery'), '1.1.0', TRUE );

	}

	if ( TRUE === get_theme_mod( 'hoverizr_enabled', FALSE ) ) {

		wp_enqueue_script( 'hoverizr-js', LIB_PATH . '/jquery/jquery.hoverizr.min.js', array('jquery'), '1.0', TRUE );

	}

	if ( TRUE === get_theme_mod( 'wow_enabled', FALSE ) ) {

		wp_enqueue_script( 'wow-js', LIB_PATH . '/wow/wow.min.js', '', '1.0.2', TRUE );

	}

	if ( TRUE === get_theme_mod( 'isotope_enabled', FALSE ) ) {

		wp_enqueue_script( 'isotope-js', LIB_PATH . '/isotope/isotope.pkgd.min.js', '', '3.0.0', TRUE );

	}

	if ( TRUE === get_theme_mod( 'lazyload_enabled', FALSE ) ) {

		wp_enqueue_script( 'lazyload-js', LIB_PATH . '/jquery/jquery.lazyload.min.js', array('jquery'), '1.9.7', TRUE );

	}

	if ( TRUE === get_theme_mod( 'gmaps_enabled', FALSE ) ) {

		wp_enqueue_script( 'maps-js', get_stylesheet_directory_uri() . '/js/maps.js', '', '0.0.1', TRUE );

	}

}
add_action( 'wp_enqueue_scripts', 'w10_load_lib_scripts' );


// Load styles
function w10_load_lib_styles() {

	if ( TRUE === get_theme_mod( 'slick_enabled', TRUE ) ) {

		wp_enqueue_style( 'slick-css', LIB_PATH . '/slick/slick.css', '', '1.6.0' );

	}

	if ( TRUE === get_theme_mod( 'magnificpopup_enabled', TRUE ) ) {

		wp_enqueue_style( 'magnificpopup-css', LIB_PATH . '/magnificpopup/magnific-popup.css', '', '1.1.0' );

	}

	if ( TRUE === get_theme_mod( 'fontawesome_enabled', TRUE ) ) {

		wp_enqueue_style( 'fontawesome-css', LIB_PATH . '/fontawesome/css/font-awesome.css', '', '4.6.3' );

	}

	if ( TRUE === get_theme_mod( 'animate_enabled', FALSE ) ) {

		wp_enqueue_style( 'animate-css', LIB_PATH . '/animate/animate.css', '', '3.5.1' );

	}

}
add_action( 'wp_enqueue_scripts', 'w10_load_lib_styles', 5 );


// Disable comments (currently on all post types), also removes any approved comments
function w10_disable_comments() {

	global $wpdb;

	if ( $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE `comment_status` != 'closed' ") > 0 ) {

		$wpdb->query( "UPDATE $wpdb->posts SET `comment_status` = 'closed'" );
		$wpdb->query( "UPDATE $wpdb->comments SET `comment_approved` = '0'" );

	}

}

if ( FALSE === get_theme_mod( 'enable_comments', TRUE ) ) {

	add_action( 'admin_init', 'w10_disable_comments' );

}


// Disable trackbacks (currently on all post types)
function w10_disable_pingbacks() {

	global $wpdb;

	if ( $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE `ping_status` != 'closed' " ) > 0 ) {

		$wpdb->query( "UPDATE $wpdb->posts SET `ping_status` = 'closed'" );

	}

}

if ( FALSE === get_theme_mod( 'enable_pingbacks', FALSE ) ) {

	add_action( 'admin_init', 'w10_disable_pingbacks' );

}


// Enqueue comment reply script
function enable_threaded_comments() {

	if ( ( ! is_admin() ) && is_singular() && comments_open() && get_option( 'thread_comments' ) === 1 ) {

		wp_enqueue_script( 'comment-reply' );

	}

}

if ( TRUE === get_theme_mod( 'enable_threaded_comments', TRUE ) ) {

	add_action( 'wp_print_scripts', 'enable_threaded_comments' );

}


// Set default blog media link to none
function w10_imagelink_setup() {

	$image_set = get_option( 'image_default_link_type' );

	if ($image_set !== 'none') {

		update_option( 'image_default_link_type', 'none' );

	}

}

if ( FALSE === get_theme_mod( 'images_as_links', FALSE ) ) {

	add_action( 'admin_init', 'w10_imagelink_setup', 10 );

}


// Remove WordPress admin bar
if ( FALSE === get_theme_mod( 'show_admin_bar', FALSE ) ) {

	add_filter( 'show_admin_bar', '__return_false' );

}


// Set Google Maps API key
if ( TRUE === get_theme_mod( 'gmaps_enabled', FALSE ) && '' !== get_theme_mod( 'gmaps_api_key', '' ) ) {

	function my_acf_init() {

		acf_update_setting( 'google_api_key', get_theme_mod( 'gmaps_api_key', '' ) );

	}
	add_action( 'acf/init', 'my_acf_init' );

}


// Clean up head
if ( TRUE === get_theme_mod( 'rm_shortlink', TRUE ) ) {

	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

}

if ( TRUE === get_theme_mod( 'rm_rsd_link', TRUE ) ) {

	remove_action( 'wp_head', 'rsd_link' );

}

if ( TRUE === get_theme_mod( 'rm_wlwmanifest_link', TRUE ) ) {

	remove_action( 'wp_head', 'wlwmanifest_link' );

}

if ( TRUE === get_theme_mod( 'rm_emoji_scripts', TRUE ) ) {

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );

}

if ( TRUE === get_theme_mod( 'rm_emoji_styles', TRUE ) ) {

	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

}
