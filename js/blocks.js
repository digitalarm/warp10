( function( $ ) {

	'use strict';

	$( function() {

		// Text slider
		if ( $().slick ) {

			$( '.text-slider__slides' ).slick({
				'speed': 1500,
				'draggable': false,
				'swipeToSlide': true,
				'autoplay': true,
				'autoplaySpeed': 5000,
				'dots': true,
			});

		}

		// Image slider
		if ( $().slick ) {

			$( '.image-slider' ).each( function() {

				var $this = $( this );

				if ( ! $this.hasClass( 'image-slider--with-nav' ) ) {

					$this.find( '.image-slider__slides' ).slick({
						'speed': 1500,
						'draggable': false,
						'swipeToSlide': true,
						'autoplay': true,
						'autoplaySpeed': 5000,
					});

				}

			});

			$( '.image-slider--with-nav' ).each( function( k ) {

				var $this = $( this );

				$this.attr( 'id', 'image-slider-' + k );
				$( $( '.image-slider-nav' )[k] ).attr( 'id', 'image-slider-nav-' + k );
				$this.find( '.image-slider__slides' ).slick({
					'speed': 1500,
					'draggable': false,
					'swipeToSlide': true,
					'autoplay': true,
					'autoplaySpeed': 5000,
					'asNavFor': '#image-slider-nav-' + k + ' .image-slider-nav__slides',
				});

				$( '#image-slider-nav-' + k ).find( '.image-slider-nav__slides' ).slick({
					'slidesToShow': 3,
					'speed': 1500,
					'draggable': false,
					'swipeToSlide': true,
					'autoplay': true,
					'autoplaySpeed': 5000,
					'centerMode': true,
					'focusOnSelect': true,
					'asNavFor': '#image-slider-' + k + ' .image-slider__slides',
				});

			});

		}

		// Video slider
		if ( $().slick ) {

			$( '.video-slider' ).each( function() {

				var $this = $( this );

				if ( ! $this.hasClass( 'video-slider--with-nav' ) ) {

					$this.find( '.video-slider__slides' ).slick({
						'speed': 1500,
						'draggable': false,
						'swipeToSlide': true,
						'autoplay': true,
						'autoplaySpeed': 5000,
					});

				}

			});

			$( '.video-slider--with-nav' ).each( function( k ) {

				var $this = $( this );

				$this.attr( 'id', 'video-slider-' + k );
				$( $( '.video-slider-nav' )[k] ).attr( 'id', 'video-slider-nav-' + k );
				$this.find( '.video-slider__slides' ).slick({
					'speed': 1500,
					'draggable': false,
					'swipeToSlide': true,
					'autoplay': true,
					'autoplaySpeed': 5000,
					'asNavFor': '#video-slider-nav-' + k + ' .video-slider-nav__slides',
				});

				$( '#video-slider-nav-' + k ).find( '.video-slider-nav__slides' ).slick({
					'slidesToShow': 3,
					'speed': 1500,
					'draggable': false,
					'swipeToSlide': true,
					'autoplay': true,
					'autoplaySpeed': 5000,
					'centerMode': true,
					'focusOnSelect': true,
					'asNavFor': '#video-slider-' + k + ' .video-slider__slides',
				});

			});

		}

		$( '.video-slider__play' ).on( 'click', function() {

			$( this ).parent().siblings( '.video-container' ).css( 'visibility', 'visible' );

		});

		// Slideshow slider
		if ( $().slick ) {

			$( '.slideshow-slider__slides' ).slick({
				'speed': 1500,
				'draggable': false,
				'swipeToSlide': true,
				'autoplay': true,
				'autoplaySpeed': 5000,
				'arrows': false,
				'dots': true,
			});

		}

		// Gallery grid
		if ( $().magnificPopup ) {

			$( '.block-gallery .grid' ).each( function() {

				$( this ).find( '.block-gallery__link' ).magnificPopup({
					'type': 'image',
					'gallery': { 'enabled': true },
				});

			});

		}

		// Carousel slider
		if ( $().slick ) {

			$( '.carousel-slider__slides' ).slick({
				'speed': 1500,
				'swipeToSlide': true,
				'autoplay': true,
				'autoplaySpeed': 5000,
				'arrows': false,
				'slidesToShow': 2,
				'slidesToScroll': 1,
			});

		}

		// WOW
		if ( typeof WOW === 'function' ) {

			new WOW().init();

		}


		// Collapsible sections
		function ddUpdate() {

			$( '.dd-list__text' ).each( function() {

				var $this = $( this );

				if ( $this.is( ':visible' ) ) {

					$this.siblings( '.dd-list__title' ).attr( 'data-open', 'true' );

				} else {

					$this.siblings( '.dd-list__title' ).attr( 'data-open', 'false' );

				}

			});

		}

		function ddToggle( ele, hideall ) {

			var $this		= $( ele );
			var $thisText	= $this.siblings( '.dd-list__text' );

			if ( hideall ) {

				$( '.dd-list__text' ).not( $thisText ).stop().slideUp( 300, function() {

					ddUpdate();

				});

			}
			if ( $thisText.css( 'display' ) === 'none' ) {

				$thisText.stop().slideDown( 300, function() {

					ddUpdate();

				});

			} else {

				$thisText.stop().slideUp( 300, function() {

					ddUpdate();

				});

			}

		}

		$( '.dd-list__text' ).hide();
		ddUpdate();

		$( '.dd-list__title' ).on( 'click', function() {

			ddToggle( this );

		});

		// Full screen image
		function setFullScreen() {

			$( '.block-full-screen-image' ).each( function() {

				var offset = $( this ).offset();
				var top = offset.top;
				var windowHeight = $( window ).height();
				var height = windowHeight - top;

				$( this ).children( '.block-full-screen-image__image' ).height( height );

			});

		}

		$( window ).on( 'resize', function() {

			setFullScreen();

		});

		setTimeout( function() {

			setFullScreen();

		}, 500 );

	});

}( jQuery ) );
