<?php
if ( isset( $_GET['author_name'] ) ) {

	$user = get_userdatabylogin( $author_name );

} else {

	$user = get_userdata( intval( $author ) );

}

get_header();
?>

<div class="page-author spacing-inside">
	<div class="container">
		<div class="author">
			<div class="author__image">
				<?php echo get_avatar( $user->ID, 120 ); ?>
			</div>

			<div class="author__content">
				<div class="author__title">
					<span class="author__name"><?php echo $user->display_name; ?></span><?php if ( $current_user->ID == $user->ID ): ?> - <a href="<?php echo get_edit_profile_url(); ?>">[<?php _e( 'Edit', 'w10' ); ?>]</a><?php endif; ?>
				</div>

				<?php if ( ! empty( $user->description ) ): echo '<p>' . $user->description . '</p>'; endif; ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
