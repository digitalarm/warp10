<?php
$image = get_sub_field( 'image' );
$next_section = time();
?>

<div class="block-full-screen-image">
	<div class="block-full-screen-image__image" style="background-image:url('<?php echo $image['url']; ?>');">
		<?php if ( get_sub_field( 'text_overlay' ) ) : ?>

			<div class="block-full-screen-image__text">
				<?php the_sub_field( 'text_overlay' ); ?>
			</div>

		<?php endif; ?>
	</div>

	<a class="block-full-screen-image__scroll" href="#<?php echo $next_section; ?>"><span class="fa fa-angle-down"></span></a>
</div>

<span id="<?php echo $next_section; ?>"></span>
