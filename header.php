<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title><?php wp_title(); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" type="text/css" media="screen">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> data-menu-state="closed">
		<?php if ( ! is_page_template( 'page-templates/page-splash.php' ) ) : ?>

			<header class="header"<?php /* data-sticky-nav="true" */ ?>>
				<div class="header__main">
					<div class="container">
						<?php if ( function_exists( 'has_custom_logo' ) ) : ?>

							<?php if ( has_custom_logo() ) : ?>

								<?php the_custom_logo(); ?>

							<?php else: ?>

								<a class="header__logo" href="<?php echo get_home_url(); ?>"><?php echo bloginfo( 'site_title' ); ?></a>

							<?php endif; ?>

						<?php else: ?>

							<a class="header__logo" href="<?php echo get_home_url(); ?>"><?php echo bloginfo( 'site_title' ); ?></a>

						<?php endif; ?>
					</div>
				</div>
				<?php if( has_nav_menu( 'header-menu' ) ) : ?>

					<div class="header__nav">
						<div class="container">
							<?php if ( has_nav_menu( 'mobile-menu' ) ) : ?>

								<div class="menu-open" aria-label="<?php _e( 'Open Menu', 'w10' ); ?>">
									<span class="menu-open__bar"></span>
									<span class="menu-open__bar"></span>
									<span class="menu-open__bar"></span>
								</div>

							<?php endif; ?>

							<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav' ) ); ?>
						</div>
					</div>

				<?php endif; ?>
			</header>

		<?php endif; ?>

		<main>
